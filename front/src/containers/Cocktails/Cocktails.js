import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import CocktailItem from "../../components/CocktailItem/CocktailItem";
import {fetchCocktailsRequest} from "../../store/actions/cocktailsActions";

const Cocktails = () => {
    const dispatch = useDispatch();
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const fetchLoading = useSelector(state => state.cocktails.fetchLoading);

    useEffect(() => {
        dispatch(fetchCocktailsRequest());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Cocktails</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : cocktails.map(cocktail => (
                        <CocktailItem
                            key={cocktail._id}
                            id={cocktail._id}
                            title={cocktail.title}
                            image={cocktail.image}
                            published={cocktail.published}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Cocktails;