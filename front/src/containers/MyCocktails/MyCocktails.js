import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMyCocktailsRequest} from "../../store/actions/cocktailsActions";
import {Card, CardHeader, CardMedia, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import imageNotAvailable from "../../assets/images/not_available.png";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const MyCocktails = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const fetchLoading = useSelector(state => state.cocktails.fetchLoading);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchMyCocktailsRequest(user._id));
    }, [dispatch, user._id]);

    let cardImage = imageNotAvailable;

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">My Cocktails</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : cocktails.length !== 0 && cocktails.map(cocktail => (
                        <Grid item xs={12} sm={6} md={6} lg={4} key={cocktail._id}>
                            <Card className={classes.card}>
                                <CardHeader title={cocktail.title}/>
                                {cocktail.image ? <CardMedia
                                    image={apiURL + '/' + cocktail.image}
                                    title={cocktail.title}
                                    className={classes.media}
                                /> : <CardMedia
                                    image={cardImage}
                                    title={cocktail.title}
                                    className={classes.media}
                                />}
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default MyCocktails;