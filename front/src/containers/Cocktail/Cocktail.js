import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktailRequest} from "../../store/actions/cocktailsActions";
import {Card, CardContent, CardHeader, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import imageNotAvailable from "../../assets/images/not_available.png";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const Cocktail = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cocktail = useSelector(state => state.cocktails.cocktail);

    useEffect(() => {
        dispatch(fetchCocktailRequest(match.params.id));
    }, [dispatch, match.params.id]);

    let cardImage = imageNotAvailable;

    return cocktail && (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={cocktail.title}/>
                {cocktail.image ? <CardMedia
                    image={apiURL + '/' + cocktail.image}
                    title={cocktail.title}
                    className={classes.media}
                /> : <CardMedia
                    image={cardImage}
                    title={cocktail.title}
                    className={classes.media}
                />}

                <CardContent>
                    <Typography variant="subtitle1">
                        Recipe: {cocktail.recipe}
                    </Typography>
                    <Typography variant="subtitle2">
                        Ingredients:
                        {cocktail.ingredients.map(cocktail => (
                            <ul key={cocktail._id}>
                                <li>{cocktail.title} - {cocktail.amount}</li>
                            </ul>
                        ))}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default Cocktail;