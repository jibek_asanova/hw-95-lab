import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {createCocktailRequest} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const CreateCocktail = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.cocktails.createCocktailError);

    const [ingredients, setIngredients] = useState([{title: '', amount: ''}]);
    const [state, setState] = useState({
        title: "",
        recipe: "",
        image: null,
    });

    const submitFormHandler = async e => {
        e.preventDefault();

        const newSt = {...state, ingredients};
        const formData = new FormData();
            Object.keys(newSt).forEach(ing => {
                if(ing === 'ingredients') {
                    formData.append(ing, JSON.stringify(newSt[ing]))
                } else {
                    formData.append(ing, newSt[ing]);
                }
            });

        await dispatch(createCocktailRequest(formData))
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    }

    const changeIngredient = (i, name, value) => {
        setIngredients(prev => {
            const ingCopy = {
                ...prev[i],
                [name]: value,
            };

            return prev.map((ing, index) => {
                if(i === index) {
                    return ingCopy;
                }
                
                return ing;
            })
        })
    }

    const deleteIngredient = (index) => {
        setIngredients(ingredients.filter((ing, i) => i !== index));
    };

    const addIngredients = () => {
        setIngredients(prev => [
            ...prev,
            {title: '', amount: ''}
        ])
    };

    return (
        <>
            <Typography variant="h4">Create cocktail</Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
                noValidate
            >
                <FormElement
                    required
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />
                <FormElement
                    multiline
                    required
                    rows={4}
                    label="Recipe"
                    name="recipe"
                    value={state.recipe}
                    onChange={inputChangeHandler}
                    error={getFieldError('recipe')}

                />
                {ingredients.map((ing, i) => (
                    <Grid
                        container
                        key={i}
                        direction="row"
                        spacing={2}
                        display="flex"
                        alignItems="center">
                        <Grid item xs={4}>
                            <TextField
                                required
                                label="Title"
                                name="title"
                                type="text"
                                value={ingredients[i].title}
                                onChange={e => changeIngredient(i, 'title', e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                required
                                type="text"
                                label="Amount"
                                name="amount"
                                value={ingredients[i].amount}
                                onChange={e => changeIngredient(i, 'amount', e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <Button type="button" onClick={() => deleteIngredient(i)}>X</Button>
                        </Grid>
                    </Grid>
                ))}
                <Grid item xs={6}>
                    <Button type="button" onClick={addIngredients}>Add Ingredient</Button>
                </Grid>
                <Grid item xs>
                    <TextField
                        required
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        // loading={loading}
                        // disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>
    );
};

export default CreateCocktail;