import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  user: null,
  registerLoading: false,
  registerError: null,
  loginError: null,
  loginLoading: false,
};

const name = 'users';

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    loginUserRequest(state) {
      state.loginLoading = true;
    },
    loginUserSuccess(state, {payload: user}) {
      state.loginError = null;
      state.loginLoading = false;
      state.user = user;
    },
    loginUserFailure(state, {payload: loginError}) {
      state.loginError = loginError;
      state.loginLoading = false;
    },
    clearErrorUser(state) {
      state.registerError = null;
      state.loginError = null;
    },
    logoutUser(state){
      state.user = null;
    },
    facebookLoginRequest(state, {payload: facebookData}) {
      state.user = facebookData;
    }
  }
});

export default usersSlice;