import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    fetchLoading: false,
    singleLoading: false,
    createCocktailLoading: false,
    createCocktailError: null,
    cocktails: [],
    cocktail: null,
};

const name = 'cocktails';


const cocktailsSlice = createSlice({
    name,
    initialState,
    reducers: {
        createCocktailRequest(state) {
          state.creatCocktailLoading = true;
        },
        createCocktailSuccess(state) {
            state.creatCocktailLoading = false;
            state.creatCocktailtError = null;
        },
        createCocktailFailure(state, {payload: error}) {
            state.creatCocktailLoading = false;
            state.creatCocktailtError = error;
        },
        fetchCocktailsRequest(state) {
            state.fetchLoading = true;
        },
        fetchCocktailsSuccess(state, {payload: cocktails}) {
            state.fetchLoading = false;
            state.cocktails = cocktails;
        },
        fetchCocktailsFailure(state) {
            state.fetchLoading = false;
        },
        fetchMyCocktailsRequest(state) {
            state.fetchLoading = true;
        },
        fetchMyCocktailsSuccess(state, {payload: cocktails}) {
            state.fetchLoading = false;
            state.cocktails = cocktails;
        },
        fetchMyCocktailsFailure(state) {
            state.fetchLoading = false;
        },
        publishCocktailRequest(state) {
            state.fetchLoading = true;
        },
        publishCocktailSuccess(state) {
            state.fetchLoading = false;
        },
        publishCocktailFailure(state) {
            state.fetchLoading = false;
        },
        deleteCocktailRequest(state) {
            state.fetchLoading = true;
        },
        deleteCocktailSuccess(state, {payload: cocktailId}) {
            state.fetchLoading = false;
            state.cocktails = state.cocktails.filter(cocktail => cocktail.id !== cocktailId);
        },
        deleteCocktailFailure(state) {
            state.fetchLoading = false;
        },
        fetchCocktailRequest(state) {
            state.singleLoading = true;
        },
        fetchCocktailSuccess(state, {payload: cocktail}) {
            state.singleLoading = false;
            state.cocktail = cocktail;
        },
        fetchCocktailFailure(state) {
            state.singleLoading = false;
        }
    }
});

export default cocktailsSlice;
