import usersSlice from "../slices/usersSlice";

export const {
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    clearErrorUser,
    logoutUser,
    facebookLoginRequest,
} = usersSlice.actions;

