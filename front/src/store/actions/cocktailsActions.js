import cocktailsSlice from "../slices/cocktailsSlice";

export const  {
    createCocktailSuccess,
    createCocktailRequest,
    createCocktailFailure,
    fetchCocktailsRequest,
    fetchCocktailsSuccess,
    fetchCocktailsFailure,
    fetchMyCocktailsRequest,
    fetchMyCocktailsSuccess,
    fetchMyCocktailsFailure,
    publishCocktailRequest,
    publishCocktailSuccess,
    publishCocktailFailure,
    deleteCocktailRequest,
    deleteCocktailSuccess,
    deleteCocktailFailure,
    fetchCocktailRequest,
    fetchCocktailSuccess,
    fetchCocktailFailure
} = cocktailsSlice.actions;