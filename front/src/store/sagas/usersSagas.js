import {put, takeEvery} from "redux-saga/effects";
import {
    facebookLoginRequest,
    loginUserFailure,
    loginUserRequest,
    loginUserSuccess,
    logoutUser
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* loginUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users/sessions', userData);
        yield put(loginUserSuccess(response.data.user));
        historyPush('/');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(loginUserFailure(error.response.data));
    }
}

export function* logoutUserSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
        historyPush('/');
    } catch (error) {
        toast.error(error.response.data.global);
    }
}

export function* facebookLoginSaga({payload: facebookData}) {
    try {
        const response = yield axiosApi.post('/users/facebookLogin', facebookData);
        yield put(loginUserSuccess(response.data.user));
        historyPush('/');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(loginUserFailure(error.response.data));
    }
}

const usersSaga = [
    takeEvery(loginUserRequest, loginUserSaga),
    takeEvery(logoutUser, logoutUserSaga),
    takeEvery(facebookLoginRequest, facebookLoginSaga),
];

export default usersSaga;