import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";
import {put, takeEvery} from "redux-saga/effects";
import {
    createCocktailFailure,
    createCocktailRequest,
    createCocktailSuccess,
    deleteCocktailFailure,
    deleteCocktailRequest,
    deleteCocktailSuccess,
    fetchCocktailFailure,
    fetchCocktailRequest,
    fetchCocktailsFailure,
    fetchCocktailsRequest,
    fetchCocktailsSuccess,
    fetchCocktailSuccess,
    fetchMyCocktailsFailure,
    fetchMyCocktailsRequest,
    fetchMyCocktailsSuccess,
    publishCocktailFailure,
    publishCocktailRequest,
    publishCocktailSuccess
} from "../actions/cocktailsActions";

export function* createCocktailSagas({payload: cocktailData}) {
    try {
        yield axiosApi.post('/cocktails', cocktailData);
        yield put(createCocktailSuccess());

        historyPush('/');
        toast.success('на рассмотрении у модератора');
    } catch (e) {
        yield put(createCocktailFailure(e.response.data));
        toast.error('Could not create cocktail');
    }
}

export function* fetchMyCocktailsSagas({payload}) {
    try {
        const response = yield axiosApi.get(`/cocktails?user=${payload}`);
        yield put(fetchMyCocktailsSuccess(response.data));
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchMyCocktailsFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchMyCocktailsFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* fetchCocktailsSagas() {
    try {
        const response = yield axiosApi.get('/cocktails');
        yield put(fetchCocktailsSuccess(response.data));
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchCocktailsFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchCocktailsFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* fetchCocktailSagas({payload: id}) {
    try {
        const response = yield axiosApi.get('/cocktails/' + id);
        yield put(fetchCocktailSuccess(response.data))
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchCocktailFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchCocktailFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* publishCocktailSagas({payload: id}) {
    try {
        yield axiosApi.put('/cocktails/' + id, null);
        yield put(publishCocktailSuccess());
        yield put(fetchCocktailsRequest());
    } catch (e) {
        yield put(publishCocktailFailure(e.response.data));
        toast.error('Could not publish')
    }
}

export function* deleteCocktail(id) {
    try {
        yield axiosApi.delete('cocktails/' + id.payload);
        yield put(deleteCocktailSuccess(id.payload));
        yield put(fetchCocktailsRequest());
        toast.success('Cocktail deleted');
    } catch (error) {
        yield put(deleteCocktailFailure(error.response.data));
        toast.error('Could not delete cocktail');
    }
}


const cocktailsSaga = [
    takeEvery(createCocktailRequest, createCocktailSagas),
    takeEvery(fetchCocktailsRequest, fetchCocktailsSagas),
    takeEvery(fetchMyCocktailsRequest, fetchMyCocktailsSagas),
    takeEvery(publishCocktailRequest, publishCocktailSagas),
    takeEvery(deleteCocktailRequest, deleteCocktail),
    takeEvery(fetchCocktailRequest, fetchCocktailSagas),
];

export default cocktailsSaga;