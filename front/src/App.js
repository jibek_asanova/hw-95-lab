import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Login from "./containers/Login/Login";
import CreateCocktail from "./containers/CreateCocktail/CreateCocktail";
import Cocktails from "./containers/Cocktails/Cocktails";
import Cocktail from "./containers/Cocktail/Cocktail";
import MyCocktails from "./containers/MyCocktails/MyCocktails";
import {useSelector} from "react-redux";

const App = () => {
  const user = useSelector(state => state.users.user);

  const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props}/> :
        <Redirect to={redirectTo}/>
  };

  return (
      <Layout>
        <Switch>
            {/*<Route path="/" exact component={Cocktails}/>*/}
            <ProtectedRoute
                path="/"
                exact
                component={Cocktails}
                isAllowed={user}
                redirectTo="/login"
            />
            <Route path="/myCocktails" component={MyCocktails}/>
            <Route path="/create" component={CreateCocktail}/>
            <Route path="/cocktails/:id" component={Cocktail}/>
            <Route path="/login" component={Login}/>
        </Switch>
      </Layout>
  );
};

export default App;
