import {Button, Card, CardActions, CardHeader, CardMedia, Grid, IconButton, makeStyles,} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {deleteCocktailRequest, publishCocktailRequest} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const CocktailItem = ({title, id, image, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);


    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }


    return (
        <>
            {user && user.role === 'admin' ?
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <Card className={classes.card}>
                        <CardHeader title={title}/>
                        <CardMedia
                            image={cardImage}
                            title={title}
                            className={classes.media}
                        />
                        <CardActions>
                            <IconButton component={Link} to={'/cocktails/' + id}>
                                <ArrowForwardIcon/>
                            </IconButton>
                            <Button onClick={() => dispatch(deleteCocktailRequest(id))}>Delete</Button>
                            {published ? <Button onClick={() => dispatch(publishCocktailRequest(id))}>Unpublished</Button> :
                                <Button onClick={() => dispatch(publishCocktailRequest(id))}>Publish</Button>}
                        </CardActions>
                    </Card>
                </Grid> : published ? <Grid item xs={12} sm={6} md={6} lg={4}>
                    <Card className={classes.card}>
                        <CardHeader title={title}/>
                        <CardMedia
                            image={cardImage}
                            title={title}
                            className={classes.media}
                        />
                        <CardActions>
                            <IconButton component={Link} to={'/cocktails/' + id}>
                                <ArrowForwardIcon/>
                            </IconButton>
                        </CardActions>
                    </Card>
                </Grid> : null}
        </>

    );
};

CocktailItem.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    image: PropTypes.string
}

export default CocktailItem;