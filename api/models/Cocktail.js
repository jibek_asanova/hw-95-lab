const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const Schema = mongoose.Schema;


const CocktailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true
  },
  recipe: {
    type: String,
    required: true
  },
  published: {
    type: Boolean,
    default: false
  },
  ingredients: [{title: String, amount: String}]
})

CocktailSchema.plugin(idvalidator);

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;