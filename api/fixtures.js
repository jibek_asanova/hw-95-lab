const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Cocktail = require("./models/Cocktail");


const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }
  const [user1, user2] = await User.create({
      email: 'jibek@gmail.com',
      password: '123',
      token: nanoid(),
      role: 'admin',
      displayName: 'Jibek',
      avatarImage: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1obN7zovv6kbr6dDQcf26ZJHFbKOCDXjMxVZh0LLrSenzaxL2'
    }, {
      email: 'bob@gmail.com',
      password: '555',
      token: nanoid(),
      role: 'user',
      displayName: 'Bob',
      avatarImage: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Rapper_B.o.B_2013.jpg'
    },
  )

  await Cocktail.create({
      user: user1,
      title: 'Summer',
      recipe: 'Summer recipe',
      image: 'fixtures/summer.jpg',
      ingredients: [{title: 'vodka', amount: '2ml'}, {title: 'rome', amount: '20ml'}],
      published: true,
    },
    {
      user: user2,
      title: 'Love',
      recipe: 'Love recipe',
      image: 'fixtures/Love.jpg',
      ingredients: [{title: 'cola', amount: '2ml'}, {title: 'tequila', amount: '20ml'}],
      published: true,
    }, {
      user: user1,
      title: 'Relax',
      recipe: 'Relax recipe',
      image: 'fixtures/Relax.jpg',
      ingredients: [{title: 'vodka', amount: '2ml'}, {title: 'rome', amount: '20ml'}],
      published: false,
    }
  )


  await mongoose.connection.close();

};

run().catch(console.error)