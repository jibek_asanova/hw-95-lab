const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Cocktail = require("../models/Cocktail");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', auth, async (req, res) => {
  try {
    const all = await Cocktail.find();
    const publishedCocktails = all.filter(cocktail => cocktail.published === true);

    if(req.query.user) {
      const userCocktails = await Cocktail.find({user: req.query.user});
      return res.send(userCocktails);
    }

    if(req.user.role === 'user') {
      return res.send(publishedCocktails)
    }
    res.send(all);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);

    if (cocktail) {
      res.send(cocktail);
    } else {
      res.status(404).send({error: 'Cocktail not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});


router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
  try {
    const cocktailData = {
      title: req.body.title,
      recipe: req.body.recipe || null,
      user: req.user._id,
      ingredients: JSON.parse(req.body.ingredients),
    };


    if (req.file) {
      cocktailData.image = 'uploads/' + req.file.filename
    }

    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();
    res.send(cocktail);

  } catch (error) {
    res.status(400).send(error);
  }
});
router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findOne({_id: req.params.id});


    cocktail.published = !cocktail.published;

    await cocktail.save();
    res.send(cocktail);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findByIdAndDelete(req.params.id);

    if (cocktail) {
      res.send(`cocktail ${cocktail.title} removed`);
    } else {
      res.status(404).send({error: 'Cocktail not found'})
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;